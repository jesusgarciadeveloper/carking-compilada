<?php

class CoreController
{

    protected function sendErrorMessage(int $codeHTTP, int $code, string $message)
    {
        http_response_code($codeHTTP);
        header('Content-Type: application/json');
        die(json_encode(["code" => $code, "message" => $message]));
    }

}
