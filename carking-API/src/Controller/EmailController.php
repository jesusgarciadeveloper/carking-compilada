<?php

use PHPMailer\PHPMailer\PHPMailer;

require_once('vendor/PHPMailer/src/PHPMailer.php');
require_once('vendor/PHPMailer/src/SMTP.php');
require_once('vendor/PHPMailer/src/Exception.php');
require_once(CTRLPATH . 'CoreController.php');

class EmailController extends CoreController
{
    function sendEmail()
    {
        try {
            $request = json_decode(file_get_contents("php://input"), true);
            $mail = new PHPMailer();
            $mail->SMTPOptions = [
                'ssl' => [
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true,
                ]
            ];
            //$mail->SMTPDebug = 2;
            $mail->IsSMTP();
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = 'ssl';
            $mail->Host = 'smtp.gmail.com';
            $mail->Port = 465; // TCP port to connect to
            $mail->CharSet = 'UTF-8';
            $mail->Username   = "carkingespana@gmail.com";
            $mail->Password   = "carking2021";
            $mail->SetFrom('carkingespana@gmail.com', 'carking españa');
            $mail->addReplyTo($request['email'], $request['nombre']);
            $mail->SMTPKeepAlive = true;
            $mail->Mailer = "smtp";

            $mail->Subject = $request['asunto'];
            $mail->IsHTML(true);
            $mail->MsgHTML($request['mensaje']);
            $mail->AddAddress($mail->Username, "carking españa");
            if (!$mail->Send()) {
                http_response_code(400);
                header('Content-Type: application/json');
                die(json_encode(['mensaje' => 'Parece que ha habido un error. Mailer Error: ' . $mail->ErrorInfo]));
            } else {
                header('Content-Type: application/json');
                die(json_encode(["mensaje" => 'Email enviado correctamente.']));
            }
        } catch (\PHPMailer\PHPMailer\Exception $e) {
            header('Content-Type: application/json');
            die(json_encode(["mensaje" => $e]));
        }
    }
}
