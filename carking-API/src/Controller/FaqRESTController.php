<?php
require_once(ENTITYPATH . "Faq.php");
require_once(REPOSITORYPATH . "FaqRepository.php");
require_once(CTRLPATH . 'CoreController.php');

class FaqRESTController extends CoreController
{

    public function __construct()
    {
    }

    public function getAllFaqs()
    {
        $faqModel = new FaqRepository();
        $arrFaqs  = $faqModel->getAllFaqs();
        $result    = [];
        foreach ($arrFaqs as $faq) {
            $result[] = $faq->getPublicData();
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function getFaqsId()
    {

        if (!isset($_GET['id'])) {
            $this->sendErrorMessage(400, 4004, "El id de la faq no existe");
        }

        $faqId = $_GET['id'];

        $faqModel = new FaqRepository();
        $faqs = $faqModel->getFaqsId($faqId);
        $result    = [];
        foreach ($faqs as $faq) {
            $result[] = $faq->getPublicData();
        }
        header('Content-Type: application/json');
        die(json_encode($result));
    }

    public function getFaq($id)
    {
        if (!isset($_GET['idfaq'])) {
            $this->sendErrorMessage(400, 4004, "El id de la faq no existe");
        }

        $faqId = $_GET['idfaq'];

        /* if (!$userSession || ($userSession->getIdusuario() != $userId && !$userSession->getIsadmin())) {
            $this->sendErrorMessage(403, 4003, "Permiso denegado para esta acción");
        } */

        $faqModel = new FaqRepository();
        $faq = $faqModel->getFaqRow($faqId);
        $faq = $faqModel->getFaq($faq);
        header('Content-Type: application/json');
        die(json_encode($faq->getPublicData()));
    }

    public function deleteFaq()
    {

        if (!isset($_GET['id'])) {
            $this->sendErrorMessage(400, 4004, "El id de la FAQ no existe");
        }

        $faqId = $_GET['id'];

        $faqModel = new FaqRepository();


        try {
            $affectedRows = $faqModel->deleteFaq($faqId);
        } catch (PDOException $e) {
            $this->sendErrorMessage(500, $e->getCode(), $e->getMessage());
        }

        if ($affectedRows <= 0) {
            $this->sendErrorMessage(200, 2001, "FAQ no encontrada");
        }

        $this->sendErrorMessage(201, 2002, "FAQ eliminada correctamente");
    }

    public function insertarFaq()
    {
        $request = json_decode(file_get_contents("php://input"), true);

        $faq = new Faq(
            0,
            $request['idautor'],
            $request['pregunta'],
            $request['respuesta']
        );

        $faqModel = new FaqRepository();

        try {
            $faqInsertada = $faqModel->insertarFaq($faq);
        } catch (PDOException $e) {
            $this->sendErrorMessage(500, $e->getCode(), $e->getMessage());
        }


        header('Content-Type: application/json');
        die(json_encode($faqInsertada->getPublicData()));
    }

    public function modificarFaq()
    {
        $faqModel = new FaqRepository();
        $request = json_decode(file_get_contents("php://input"), true);
        try {
            $faqBD = $faqModel->getFaqRow($request["idfaq"]);
            if (empty($faqBD)) {
                $this->sendErrorMessage(400, 2001, "Faq no encontrada");
            }
            $faqBD = $faqModel->getFaq($faqBD);
            $faqBD->setPregunta($request["pregunta"]);
            $faqBD->setRespuesta($request["respuesta"]);
            $affectedRows = $faqModel->updateFaq($faqBD);
        } catch (Exception $e) {
            $this->sendErrorMessage((int)$e->getCode(), (int) $e->getCode(), $e->getMessage());
        }

        http_response_code(201);
        header('Content-Type: application/json');
        die(json_encode($faqBD->getPublicData()));
    }
}
