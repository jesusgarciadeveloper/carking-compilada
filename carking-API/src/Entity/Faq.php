<?php
class Faq
{
    /**
     * @var int
     */
    private $idfaq;

    /**
     * @var string
     */
    private $pregunta;

    /**
     * @var string
     */
    private $respuesta;

    /**
     * @var int
     *
     */
    private $idautor;

    public function getIdfaq(): int
    {
        return $this->idfaq;
    }

    public function getPregunta(): string
    {
        return $this->pregunta;
    }

    public function setPregunta(string $pregunta): self
    {
        $this->pregunta = $pregunta;

        return $this;
    }

    public function getRespuesta(): string
    {
        return $this->respuesta;
    }

    public function setRespuesta(string $respuesta): self
    {
        $this->respuesta = $respuesta;

        return $this;
    }

    public function getIdAutor(): int
    {
        return $this->idautor;
    }

    public function setIdAutor(int $idautor): self
    {
        $this->idautor = $idautor;

        return $this;
    }

    /**
     * Constructor
     */
    public function __construct(
        int $idfaq,
        int $idautor,
        string $pregunta,
        string $respuesta
    ) {
        $this->idfaq = $idfaq;
        $this->idautor = $idautor;
        $this->pregunta = $pregunta;
        $this->respuesta = $respuesta;
    }

    public function getPublicData(): array
    {
        return [
            "idfaq" => $this->idfaq,
            "idautor" => $this->idautor,
            "pregunta" => $this->pregunta,
            "respuesta" => $this->respuesta
        ];
    }
}
