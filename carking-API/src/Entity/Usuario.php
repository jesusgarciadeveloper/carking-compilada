<?php

class Usuario
{
    /**
     * @var int
     *
     */
    private $idusuario;

    /**
     * @var string
     *
     */
    private $dni;

    /**
     * @var string
     *
     */
    private $alias;

    /**
     * @var string
     *
     */
    private $nombre;

    /**
     * @var string
     *
     */
    private $apellidos;

    /**
     * @var \DateTime
     *
     */
    private $fechanac;

    /**
     * @var string
     *
     */
    private $direccion;

    /**
     * @var string|null
     *
     */
    private $numero;

    /**
     * @var string
     *
     */
    private $poblacion;

    /**
     * @var string
     *
     */
    private $provincia;

    /**
     * @var string
     *
     */
    private $codigopostal;

    /**
     * @var string|null
     *
     */
    private $tel;

    /**
     * @var string
     *
     */
    private $email;

    /**
     * @var string
     *
     */
    private $pass;

    /**
     * @var bool
     *
     */
    private $isadmin = 0;

    /**
     * @var \DateTime
     *
     */
    private $fechareg;

    /**
     * Constructor
     *
     * /**
     * User constructor.
     * @param string $nombre
     * @param string $apellidos
     * @param string $dni
     * @param string $alias
     * @param DateTimeImmutable $fechanac
     * @param string $direccion
     * @param string $numero
     * @param string $poblacion
     * @param string $provincia
     * @param string $codigopostal
     * @param string $tel
     * @param string $email
     * @param string $pass
     * @param bool $isadmin
     */
    public function __construct(
        int $idusuario,
        string $nombre,
        string $apellidos,
        string $dni,
        string $alias,
        DateTimeImmutable $fechaNac,
        string $direccion,
        string $numero,
        string $poblacion,
        string $provincia,
        string $codigoPostal,
        string $tel,
        string $email,
        bool $isAdmin,
        string $pass
    ) {
        $this->idusuario = $idusuario;
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->dni = $dni;
        $this->alias = $alias;
        $this->email = $email;
        $this->fechanac = $fechaNac;
        $this->isadmin = $isAdmin;
        $this->direccion = $direccion;
        $this->numero = $numero;
        $this->poblacion = $poblacion;
        $this->provincia = $provincia;
        $this->codigopostal = $codigoPostal;
        $this->tel = $tel;
        $this->pass = $pass;
    }

    public function getIdusuario(): ?int
    {
        return $this->idusuario;
    }

    public function getDni(): ?string
    {
        return $this->dni;
    }

    public function setDni(string $dni): self
    {
        $this->dni = $dni;

        return $this;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function setAlias(string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellidos(): ?string
    {
        return $this->apellidos;
    }

    public function setApellidos(string $apellidos): self
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    public function getFechanac(): ?\DateTimeInterface
    {
        return $this->fechanac;
    }

    public function setFechanac(\DateTimeInterface $fechanac): self
    {
        $this->fechanac = $fechanac;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(?string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getPoblacion(): ?string
    {
        return $this->poblacion;
    }

    public function setPoblacion(string $poblacion): self
    {
        $this->poblacion = $poblacion;

        return $this;
    }

    public function getProvincia(): ?string
    {
        return $this->provincia;
    }

    public function setProvincia(string $provincia): self
    {
        $this->provincia = $provincia;

        return $this;
    }

    public function getCodigopostal(): ?string
    {
        return $this->codigopostal;
    }

    public function setCodigopostal(string $codigopostal): self
    {
        $this->codigopostal = $codigopostal;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(?string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPass()
    {
        return $this->pass;
    }

    public function setPass(string $pass): self
    {
        $this->pass = $pass;

        return $this;
    }

    public function getIsadmin(): ?bool
    {

        return $this->isadmin;
    }

    public function setIsadmin(bool $isadmin): self
    {
        $this->isadmin = $isadmin;

        return $this;
    }

    public function getFechareg(): ?\DateTimeInterface
    {
        return $this->fechareg;
    }

    public function setFechareg(?\DateTimeInterface $fechareg): self
    {
        if ($fechareg) {
            $this->fechareg = $fechareg;
            return $this;
        }
        $fecha = new \DateTime();
        $this->fechareg;
    }

    public function getPublicData(): array
    {
        return [
            'idusuario'     => $this->idusuario,
            'nombre'        => $this->nombre,
            'apellidos'     => $this->apellidos,
            'dni'           => $this->dni,
            'alias'         => $this->alias,
            'email'         => $this->email,
            'idusuario'     => $this->idusuario,
            'fechanac'      => $this->fechanac->getTimestamp() * 1000,
            'isadmin'       => $this->isadmin,
            'direccion'     => $this->direccion,
            'numero'        => $this->numero,
            'poblacion'     => $this->poblacion,
            'provincia'     => $this->provincia,
            'codigopostal'  => $this->codigopostal,
            'pass'          => $this->pass,
            'tel'           => $this->tel
        ];
    }
}
