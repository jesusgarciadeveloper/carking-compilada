<?php

/**
 * Vehiculo
 */
class Vehiculo
{
    /**
     * @var int
     *
     */
    private $idvehiculo;

    /**
     * @var string
     *
     */
    private $matricula;

    /**
     * @var string
     *
     */
    private $marca;

    /**
     * @var string
     *
     */
    private $modelo;

    /**
     * @var string
     *
     */
    private $carroceria;

    /**
     * @var \DateTimeImmutable
     *
     */
    private $anyo;

    /**
     * @var string
     *
     */
    private $longitud = '5000.00';

    /**
     * @var string
     *
     */
    private $anchura = '2500.00';

    /**
     * @var array
     */
    private $propietarios = [];

    /**
     * Constructor
     */
    public function __construct(
        int $idvehiculo,
        string $matricula,
        string $marca,
        string $modelo,
        string $carroceria,
        DateTimeImmutable $anyo
    ) {
        $this->idvehiculo = $idvehiculo;
        $this->matricula = $matricula;
        $this->marca = $marca;
        $this->modelo = $modelo;
        $this->carroceria = $carroceria;
        $this->anyo = $anyo;
    }



    public function addPropietario(int $propietario)
    {
        if (in_array($propietario, $this->propietarios)) {

            $this->propietarios[] = $propietario;
        }

        return $this;
    }

    public function removePropietario(int $propietario)
    {
        if (in_array($propietario, $this->propietarios)) {

            unset($this->propietarios[array_search($propietario, $this->propietarios)]);
        }
        return $this;
    }

    public function getPublicData(): array
    {
        return [
            'idvehiculo' => $this->idvehiculo,
            'matricula' => $this->matricula,
            'marca' => $this->marca,
            'modelo' => $this->modelo,
            'carroceria' => $this->carroceria,
            'anyo' => $this->anyo,
            'propietarios' => $this->propietarios
        ];
    }


    /**
     * Get the value of anchura
     *
     * @return  string
     */
    public function getAnchura()
    {
        return $this->anchura;
    }

    /**
     * Set the value of anchura
     *
     * @param  string  $anchura
     *
     * @return  self
     */
    public function setAnchura(string $anchura)
    {
        $this->anchura = $anchura;

        return $this;
    }

    /**
     * Get the value of propietarios
     *
     * @return  array
     */
    public function getPropietarios()
    {
        return $this->propietarios;
    }

    /**
     * Set the value of propietarios
     *
     * @param  array  $propietarios
     *
     * @return  self
     */
    public function setPropietarios(array $propietarios)
    {
        $this->propietarios = $propietarios;

        return $this;
    }


    /**
     * Get the value of idvehiculo
     *
     * @return  int
     */
    public function getIdvehiculo()
    {
        return $this->idvehiculo;
    }

    /**
     * Get the value of matricula
     *
     * @return  string
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * Set the value of matricula
     *
     * @param  string  $matricula
     *
     * @return  self
     */
    public function setMatricula(string $matricula)
    {
        $this->matricula = $matricula;

        return $this;
    }

    /**
     * Get the value of marca
     *
     * @return  string
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set the value of marca
     *
     * @param  string  $marca
     *
     * @return  self
     */
    public function setMarca(string $marca)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get the value of modelo
     *
     * @return  string
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set the value of modelo
     *
     * @param  string  $modelo
     *
     * @return  self
     */
    public function setModelo(string $modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get the value of carroceria
     *
     * @return  string
     */
    public function getCarroceria()
    {
        return $this->carroceria;
    }

    /**
     * Set the value of carroceria
     *
     * @param  string  $carroceria
     *
     * @return  self
     */
    public function setCarroceria(string $carroceria)
    {
        $this->carroceria = $carroceria;

        return $this;
    }

    /**
     * Get the value of anyo
     *
     * @return  \DateTimeImmutable
     */
    public function getAnyo()
    {
        return $this->anyo;
    }

    /**
     * Set the value of anyo
     *
     * @param  \DateTimeImmutable  $anyo
     *
     * @return  self
     */
    public function setAnyo(\DateTimeImmutable $anyo)
    {
        $this->anyo = $anyo;

        return $this;
    }

    /**
     * Get the value of longitud
     *
     * @return  string
     */
    public function getLongitud()
    {
        return $this->longitud;
    }

    /**
     * Set the value of longitud
     *
     * @param  string  $longitud
     *
     * @return  self
     */
    public function setLongitud(string $longitud)
    {
        $this->longitud = $longitud;

        return $this;
    }
}
