<?php

require_once(ENTITYPATH . "Parking.php");

class ParkingRepository extends CoreModel
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getParkingRow(int $id): array
    {
        $sql = "SELECT * FROM parking WHERE idparking = :id";
        $response = parent::getRow($sql, array("id" => $id));

        return $response;
    }

    public function getParking(array $parkingRow): Parking
    {
        return $this->generateParking($parkingRow);
    }

    private function generateParking(array $request): Parking
    {
        return new Parking(
            $request['idparking'],
            $request['nombre'],
            $request['direccion'],
            $request['numero'],
            $request['poblacion'],
            $request['provincia'],
            $request['codigopostal'],
            $request['tel'],
            $request['web'],
            $request['correo'],
            $request['preciomin'],
            $request['preciomedh'],
            $request['precioh'],
            $request['preciodia'],
            $request['preciomaxd'],
            new DateTimeImmutable($request['apertura']),
            new DateTimeImmutable($request['cierre']),
            $request['estadocierre']
        );
    }

    public function getAllParkings(): array
    {
        $query    = 'SELECT * FROM parking';
        $response = parent::getArrayRows($query);

        $result = [];
        foreach ($response as $parking) {
            $result[] = $this->generateParking($parking);
        }

        return $result;
    }

    public function getParkingsId(int $id): array
    {
        $sql = "SELECT * FROM parking WHERE idparking LIKE '%$id%'";
        $response = parent::getArrayRows($sql);
        $result = [];
        foreach ($response as $par) {
            $result[] = $this->generateParking($par);
        }

        return $result;
    }



    public function insertarParking(Parking $parking): Parking
    {
        $nombre   = $parking->getNombre();
        $direccion   = $parking->getDireccion();
        $numero   = $parking->getNumero();
        $poblacion   = $parking->getPoblacion();
        $provincia   = $parking->getProvincia();
        $codigopostal   = $parking->getCodigopostal();
        $tel   = $parking->getTel();
        $web   = $parking->getWeb();
        $correo   = $parking->getCorreo();
        $preciomin   = $parking->getPreciomin();
        $preciomedh   = $parking->getPreciomedh();
        $precioh   = $parking->getPrecioh();
        $preciodia   = $parking->getPreciodia();
        $preciomaxd   = $parking->getPreciomaxd();

        $apertura   = $parking->getApertura()->setTimezone(new DateTimeZone("Europe/Madrid"))->format("H:i:s");
        $cierre   = $parking->getCierre()->setTimezone(new DateTimeZone("Europe/Madrid"))->format("H:i:s");
        $estadocierre   = $parking->getEstadocierre();

        $sql = "INSERT INTO `parking`(`nombre`, `direccion`, `numero`, `poblacion`, `provincia`, `codigopostal`, `tel`, `web`, `correo`, `preciomin`, `preciomedh`, `precioh`, `preciodia`, `preciomaxd`, `apertura`, `cierre`, `estadocierre`) 
                VALUES (
                        '$nombre', 
                        '$direccion',
                        '$numero',
                        '$poblacion',
                        '$provincia',
                        '$codigopostal',
                        '$tel',
                        '$web',
                        '$correo',
                        '$preciomin',
                        '$preciomedh',
                        '$precioh',
                        '$preciodia',
                        '$preciomaxd',
                        '$apertura',
                        '$cierre',
                        '$estadocierre'
                        )";

        $numFilas = parent::execQuery($sql);

        if ($numFilas > 0) {
            return $this->generateParking(parent::getRow("SELECT * FROM parking WHERE idparking = " . $this->connection->lastInsertId()));
        }
    }

    public function updateParking(Parking $parking): int
    {
        $sql = "UPDATE `parking` 
                SET `nombre`=:nombre,
                    `direccion`=:direccion,
                    `numero`=:numero,
                    `poblacion`=:poblacion,
                    `provincia`=:provincia,
                    `codigopostal`=:codigopostal,
                    `tel`=:tel,
                    `web`=:web,
                    `correo`=:correo,
                    `preciomin`=:preciomin,
                    `preciomedh`=:preciomedh,
                    `precioh`=:precioh,
                    `preciodia`=:preciodia,
                    `preciomaxd`=:preciomaxd,
                    `apertura`=:apertura,
                    `cierre`=:cierre,
                    `estadocierre`=:estadocierre 
                WHERE idparking = :uuid";

        $array = array(
            "nombre" => $parking->getNombre(),
            "direccion"   => $parking->getDireccion(),
            "numero"   => $parking->getNumero(),
            "poblacion"   => $parking->getPoblacion(),
            "provincia"   => $parking->getProvincia(),
            "codigopostal"   => $parking->getCodigopostal(),
            "tel"   => $parking->getTel(),
            "web"   => $parking->getWeb(),
            "correo"   => $parking->getCorreo(),
            "preciomin"   => $parking->getPreciomin(),
            "preciomedh"   => $parking->getPreciomedh(),
            "precioh"   => $parking->getPrecioh(),
            "preciodia"   => $parking->getPreciodia(),
            "preciomaxd"   => $parking->getPreciomaxd(),
            "apertura"   => $parking->getApertura()->setTimezone(new DateTimeZone("Europe/Madrid"))->format("H:i:s"),
            "cierre"   => $parking->getCierre()->setTimezone(new DateTimeZone("Europe/Madrid"))->format("H:i:s"),
            "estadocierre"   => $parking->getEstadocierre(),
            "uuid"      => $parking->getIdparking()
        );


        return parent::execQuery($sql, $array);
    }

    public function deleteParking(string $parkingId): int
    {
        $sql = "DELETE FROM parking WHERE idparking = :uuid";
        $response = parent::execQuery($sql, [":uuid" => $parkingId]);
        return $response;
    }
}
