<?php

require_once(ENTITYPATH . "Usuario.php");
class UsuarioRepository extends CoreModel
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getLoginUser(string $email, string $pass)
    {
        $query = 'SELECT * FROM usuario WHERE email = :email AND pass = :pass';
        $response = parent::getRow($query, [':email' => $email, ':pass' => $pass]);
        if (count($response) > 0) {
            return $this->generateUser($response);
        }
        return false;
    }

    public function deleteUser(string $userId): int
    {
        $sql = "DELETE FROM usuario WHERE idusuario = :uuid";
        $response = parent::execQuery($sql, [":uuid" => $userId]);
        return $response;
    }

    public function updateUser(Usuario $user): int
    {
        $birth = $user->getFechanac()->format("Y-m-d H:i:s");
        $isadmin = intval($user->getIsadmin());

        $sql = "UPDATE usuario 
        SET nombre = :nom,
            apellidos = :ape,
            dni = :dni,
            alias = :alias, 
            fechanac = '$birth',
            direccion = :dir,
            numero = :num,
            poblacion = :pob,
            provincia = :prov, 
            email = :email,
            tel = :tel,
            pass = :pass,
            isadmin = '$isadmin'
        WHERE idusuario = :uuid";

        $array = array(
            "uuid"  => $user->getIdusuario(),
            "nom"   => $user->getNombre(),
            "ape"   => $user->getApellidos(),
            "dni"   => $user->getDni(),
            "alias" => $user->getAlias(),
            "dir"   => $user->getDireccion(),
            "num"   => $user->getNumero(),
            "pob"   => $user->getPoblacion(),
            "prov"  => $user->getProvincia(),
            "email" => $user->getEmail(),
            "tel"   => $user->getTel(),
            "pass"  => $user->getPass()
        );


        return parent::execQuery($sql, $array);
    }

    public function insertUser(Usuario $user): Usuario
    {
        $birth = $user->getFechanac()->format("Y-m-d H:i:s");
        $isadmin = intval($user->getIsadmin());

        $nom   = $user->getNombre();
        $ape   = $user->getApellidos();
        $dni   = $user->getDni();
        $alias = $user->getAlias();
        $dir   = $user->getDireccion();
        $num   = $user->getNumero();
        $pob   = $user->getPoblacion();
        $prov  = $user->getProvincia();
        $cod   = $user->getCodigopostal();
        $email = $user->getEmail();
        $tel   = $user->getTel();
        $pass  = $user->getPass();
        $sql = "INSERT INTO `usuario`( 
            `dni`, `alias`, `nombre`, `apellidos`, 
            `fechanac`, `direccion`, `numero`, 
            `poblacion`, `provincia`, `codigopostal`, 
            `tel`, `email`, `pass`, `isadmin`) 
                VALUES (
                        '$dni',
                        '$alias', 
                        '$nom',
                        '$ape',
                        '$birth',
                        '$dir',
                        $num,
                        '$pob',
                        '$prov',
                        '$cod', 
                        '$tel', 
                        '$email',
                        '$pass', 
                        $isadmin)";
        $numFilas = parent::execQuery($sql);

        if ($numFilas > 0) {
            return $this->generateUser(parent::getRow("SELECT * FROM usuario WHERE idusuario = " . $this->connection->lastInsertId()));
        }
    }

    public function getAllUsers(): array
    {
        $query    = 'SELECT * FROM usuario';
        $response = parent::getArrayRows($query);

        $result = null;
        foreach ($response as $user) {
            $result[] = $this->generateUser($user);
        }

        return $result;
    }

    private function generateUser(array $request): Usuario
    {
        return new Usuario(
            $request['idusuario'],
            $request['nombre'],
            $request['apellidos'],
            $request['dni'],
            $request['alias'],
            new DateTimeImmutable($request['fechanac']),
            $request['direccion'],
            $request['numero'],
            $request['poblacion'],
            $request['provincia'],
            $request['codigopostal'],
            $request['tel'],
            $request['email'],
            $request['isadmin'],
            $request['pass']
        );
    }

    public function getUserRow(int $id): array
    {
        $sql = "SELECT * FROM usuario WHERE idusuario = :id";
        $response = parent::getRow($sql, array("id" => $id));

        return $response;
    }
    public function getUserAliasRow(string $alias): array
    {
        $sql = "SELECT * FROM usuario WHERE alias LIKE '%" . $alias . "%'";
        $response = parent::getArrayRows($sql);
        $result = [];
        foreach ($response as $user) {
            $result[] = $this->generateUser($user);
        }
        return $result;
    }

    public function getUser(array $userRow): Usuario
    {
        return $this->generateUser($userRow);
    }

    public function checkIfAliasExist(string $alias): array
    {
        $query = "SELECT * FROM usuario WHERE alias = :alias";
        $response = parent::getRow($query, [':alias' => $alias]);

        return $response;
    }

    public function checkIfEmailExist(string $email): array
    {
        $query = "SELECT * FROM usuario WHERE email = :email";
        $response = parent::getRow($query, [':email' => $email]);

        return $response;
    }

    public function getNumAdmin(): int
    {
        $query = "SELECT * FROM usuario WHERE isadmin = 1";
        $response = count(parent::getArrayRows($query));

        return $response;
    }
}
