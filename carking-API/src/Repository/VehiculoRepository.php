<?php

require_once(ENTITYPATH . "Vehiculo.php");
class VehiculoRepository extends CoreModel
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getCarsFromUser($idusuario)
    {
        $sql = "SELECT * FROM vehiculo WHERE idvehiculo IN (SELECT idvehiculo 
                                                            FROM titularidadcoche
                                                            WHERE idusuario = :id)";

        $response = parent::getArrayRows($sql, array("id" => $idusuario));

        $result = [];
        foreach ($response as $vehiculo) {
            $result[] = $this->generateVehiculo($vehiculo);
        }
        return $result;
    }

    public function generateVehiculo(array $request): Vehiculo
    {
        return new Vehiculo(
            $request['idvehiculo'],
            $request['matricula'],
            $request['marca'],
            $request['modelo'],
            $request['carroceria'],
            new DateTimeImmutable($request['anyo'])
        );
    }

    public function updateVehiculo(Vehiculo $vehiculo): int
    {
        $anyo = $vehiculo->getAnyo()->format("Y-m-d H:i:s");

        $sql = "UPDATE vehiculo 
        SET matricula = :mat,
            marca = :mar,
            modelo = :mod,
            carroceria = :car, 
            anyo = '$anyo'
        WHERE idvehiculo = :uuid";

        $array = array(
            "uuid"  => $vehiculo->getIdvehiculo(),
            "mat"   => $vehiculo->getMatricula(),
            "mar"   => $vehiculo->getMarca(),
            "mod"   => $vehiculo->getModelo(),
            "car"   => $vehiculo->getCarroceria()
        );
        return parent::execQuery($sql, $array);
    }

    public function insertTitularidad(int $idvehiculo, int $idtitular): bool
    {


        $sql = "INSERT INTO `titularidadcoche`( 
            `idusuario`, `idvehiculo`) 
                VALUES (
                        '$idtitular',
                        '$idvehiculo')";

        $numFilas = parent::execQuery($sql);

        if ($numFilas > 0) {
            return true;
        }
        return false;
    }


    public function insertVehiculo(Vehiculo $vehiculo): Vehiculo
    {
        $matricula   = $vehiculo->getMatricula();
        $marca   = $vehiculo->getMarca();
        $modelo   = $vehiculo->getModelo();
        $carroceria   = $vehiculo->getCarroceria();
        $fecha   = $vehiculo->getAnyo()->format("Y-m-d H:i:s");

        $sql = "INSERT INTO `vehiculo`( 
            `matricula`, `marca`, `modelo`, `carroceria`, `anyo`) 
                VALUES (
                        '$matricula',
                        '$marca',
                        '$modelo',
                        '$carroceria', 
                        '$fecha')";

        $numFilas = parent::execQuery($sql);

        if ($numFilas > 0) {
            return $this->generateVehiculo(parent::getRow("SELECT * FROM vehiculo WHERE idvehiculo = " . $this->connection->lastInsertId()));
        } else {
            return null;
        }
    }

    public function getNumPropietarios(string $vehiculoId): int
    {
        $sql = "SELECT * FROM titularidadcoche WHERE idvehiculo = :uuid";
        $response = count(parent::getArrayRows($sql, [":uuid" => $vehiculoId]));
        return $response;
    }

    public function deleteVehiculo(string $vehiculoId): int
    {
        $sql = "DELETE FROM vehiculo WHERE idvehiculo = :uuid";
        $response = parent::execQuery($sql, [":uuid" => $vehiculoId]);
        return $response;
    }

    public function deleteTitularidadVehiculo(string $vehiculoId, string $usuarioId): int
    {
        $sql = "DELETE FROM titularidadcoche WHERE idvehiculo = :uuidv AND idusuario = :uuidu";
        $response = parent::execQuery($sql, [":uuidv" => $vehiculoId, ":uuidu" => $usuarioId]);
        return $response;
    }

    public function existeMatricula(string $matricula): array
    {
        $query = "SELECT * FROM vehiculo WHERE matricula = :mat";
        $response = parent::getRow($query, [':mat' => $matricula]);

        return $response;
    }

    public function getVehiculoRow(int $id): array
    {
        $sql = "SELECT * FROM vehiculo WHERE idvehiculo = :id";
        $response = parent::getRow($sql, array("id" => $id));

        return $response;
    }

    public function getVehiculo(array $vehiculoRow): Vehiculo
    {
        return $this->generateVehiculo($vehiculoRow);
    }

    public function checkIfMatriculaExist(string $matricula): array
    {
        $query = "SELECT * FROM vehiculo WHERE matricula = :matricula";
        $response = parent::getRow($query, [':matricula' => $matricula]);

        return $response;
    }
}
