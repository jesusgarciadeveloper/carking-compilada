<?php

/**
 * CONSTANT ROUTES DEFINITION
 * THE ROUTES MUST BE RELATIVES TO ROOT FOLDER
 */

define('CTRLPATH', 'src/Controller/');
define('REPOSITORYPATH', 'src/Repository/');
define('ENTITYPATH', 'src/Entity/');
define('HELPERPATH', 'src/Helper/');
define('UPLOADFOLDER', 'assets/upload/');
define('ROOTFOLDER', 'https://carking.com/carking-API/');

define('DB_CONFIG', [
    'db_host' => 'localhost',
    'db_user' => 'carking',
    'db_pass' => 'carking',
    'db_name' => 'carking'
]);
